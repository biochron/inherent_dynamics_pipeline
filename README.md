# Data-to-Networks Pipeline Code

This Python module implements functions for running the Data-to-Networks pipeline by managing the installation of all required modules and repositories, implementing the 'stitching' routines used to combine the steps of the pipeline, and providing a standardized method to initiate the pipeline from start to finish.

## Getting Started

The pipeline codebase consists of Python 3 modules (and C bindings) maintained in several stand-alone repositories.

### 1) Install Prerequisites

Several of the Data-to-Networks modules also make use of MPI for parallelization.  Prior to installation be sure to have an updated Python and MPI installation:

* Anaconda or Miniconda
    

* MPI
    If you do not have `MPI` installed:
    ```bash
    For MacOS
    $ brew install openmpi
    $ mpiexec --version
    
    For Ubuntu
    $ sudo apt-get install openmpi-bin
    $ mpiexec --version
    ```
    MPI can alternatively be installed into your `conda` environment after it is built below during installation. Insert the line
    ```bash
    $ conda install -y -c conda-forge openmpi
    ``` 
    after 
    ```bash
    $ conda activate dat2net
    ```

<!-- * cmake  
    If you do not have `cmake` installed:
    ```
    For MacOS
    $ brew install cmake
    $ cmake --version
    
    For Ubuntu
    $ sudo apt-get -y install cmake
    $ cmake --version
    ``` -->

### 2) Install Pipeline 

To ensure compatibility across platforms and with required packages, a conda environment should first be created using the supplied `conda_req.yml` file and activated before running the custom Python installation script. 
```bash
$ conda env create -f conda_req.yml
$ conda activate dat2net
$ python install.py
```
These steps will install the required Python packages to the environment `dat2net` so as to not interfere with existing packages you may have installed. The installation script clones several public Gitlab repositories. Local copies of these repositories will be added to the path containing `install.py`. Please ensure that the conda environment `dat2net` is active prior to installing or running any part of the pipeline.

### 3) Run Tests
Tests of `min_interval_posets`, `dsgrn_net_gen`, `dsgrn_net_query`, and `dsgrn_utilities` can be performed by running 
```bash
$ pytest
```
from within the respective `/tests/` subfolders. These tests are run on installation.

<!-- #### 4) Troubleshooting for Linux

It is common to have issues with C tooling during installation. An alternative is to try using C tools that are installed through `conda` instead of through a package manager. This has been used as a solution for installing on Ubuntu and on remote Linux clusters (with mixed success). The following commands install C and C++ compilers within your `dat2net` conda environment. 

```text
$ conda activate dat2net
$ conda install -y -c conda-forge gcc_linux-64 gxx_linux-64
```

In order for these tools to be visible for installation, you'll need to symlink them into the common names `gcc` and `g++`. 
```
$ ln -s <your_path>/anaconda3/envs/dat2net/bin/x86_64-conda_cos6-linux-gnu-gcc <your_path>/anaconda3/envs/dat2net/bin/gcc
$ ln -s <your_path>/anaconda3/envs/dat2net/bin/x86_64-conda_cos6-linux-gnu-g++ <your_path>/anaconda3/envs/dat2net/bin/g++
```

**Note:** These conda compilers will be used whenever the `dat2net` conda environment is active. To use system-level C tools, deactivate the `dat2net` conda environment, `conda deactivate`.

**Note:** The executable names `x86_64-conda_cos6-linux-gnu-g**` may change and have to be altered in the above commands.

After the compilers are linked, install/reinstall C tools.
```text
$ conda install -y -c conda-forge cmake openmpi mpi4py
```

Now try installing again:
```text
$ python install.py
``` -->



## Running the Pipeline

The command to run the pipeline is 
```bash
$ source activate dat2net
$ cd inherent_dynamics_pipeline
$ python src/dat2net.py <configfile.txt>
```
An example configuration file is given in 
```bash
tests/synnet6/synnet6_config.txt
```

### Configuration Files

The Data-to-Networks module takes as input a single configuration file that adheres to the ConfigObj standard (see http://www.voidspace.org.uk/python/configobj.html for details) and should contain one or more of the following sections:

1) **[dlxjtk_arguments]**: Arguments needed to generate a list of likely transcriptional regulatory elements (nodes) using the DLxJTK algorithm to rank genes  
2) **[lempy_arguments]**: Arguments needed to generate a list of likely transcriptional regulatory interactions (edges) using the LEM algorithm to rank regulation models
3) **[netgen_arguments]**: Arguments needed to generate a seed network from a ranked list of edges
4) **[netper_arguments]**: Arguments needed to generate many potential networks using random perturbations of a seed network
5) **[netquery_arguments]**: Arguments needed to score networks according to how robustly they can produce the dynamics observed in the data

The pipeline can run any subset of the above 5) sections by specifying in the pipeline configuration file those sections and associated arguments that should be run. 

#### Main Arguments

There are three required and two optional arguments which are used by multiple steps of the pipeline and must be included at the top of the configuration file before any subsections associated with steps of the pipeline:

* **data_file**: path to a (list of) .tsv dataframe(s) containing time series expression data (See **Data Inputs** section for details)
* **annotation_file**: path to a .tsv dataframe containing flags of gene function (See **Data Inputs** section for details)
* **output_dir**: path to top-level directory in which to save all output
* **num_proc**: an integer specifying the number of MPI processes to use (note that you must choose a number of cores that your machine actually has; oversubscribing will throw an error)
* **verbose** (optional): boolean flag indicating if pipeline progress is printed to the command prompt (Default: True)
* **IDVconnection** (optional): boolean flag indicating if user-specificed files in the configuration file should be copied into the **output_dir**. This allows for easy connection of the **output_dir** to the IDVisualizer (Default: False)
#### Essential Data Input Arguments

#### 1) [dlxjtk_arguments]

`Step 1 (Node Finding)` only requires as input the collection of gene expression time series in order to rank genes and produce a gene list to be fed into `Step 2) (Edge Finding)`. In the absence of prior biological knowledge or to augment existing knowledge about the important transcriptional regulatory elements, the DLxJTK algorithm can be run to rank genes according to how likely they are to be a core gene responsible for producing the observed transcript expression program.

* **periods**: a list of periods at which to test for oscillatory dynamics using the JTK algorithm

**Note**: If a list of datasets is supplied to **data_file**, then a list of lists of periods must be given here. The order of lists must follow the order of data files in **data_file**. Furthermore, the format of the **periods** argument will change if one datafile is supplied or if multiple are supplied. 
* One datafile example: 
    * **data_file**: datafile1
    * **periods**: 72, 74, 76
* Three datafiles example: 
    * **data_file**: datafile1, datafile2, datafile3
    * **periods**: [72, 74, 76], [96, 98, 100], [67, 69, 71]

**Note**: The DLxJTK formula uses to each gene's de Lichtenberg's regulation strength and JTK-CYCLE's periodicity significance scores (p-values). If multiple datasets are supplied, then the maximum p-value for each measure across all datasets for each gene is used to compute the DKxJTK score for said gene. This is the default method but two other methods can be used: the minimum p-value is taken or the Fisher's method is used. See DLxJTK's **Optional Arguments** section for how to specify a particular method in the configuration file.


#### 2) [lempy_arguments]
`Step 2) (Edge Finding)` of the pipeline requires as input the collection of gene expression time series for a list of genes which are assumed to be core regulatory elements. In the absence of prior biological knowledge or to augment existing knowledge about the functional regulatory interactions between nodes, the LEM algorithm can be run to rank regulatory models according to how likely they are to be responsible for producing the observed transcript expression program.

* **gene_list_file**: a list of genes specifying the potential core genes. The first item in the list must be some string, for example, "gene_name".

**Note**: If `Step 1 (Node Finding)` was run, the argument **gene_list_file** is automatically generated and will be used in `Step 2) (Edge Finding)`. However, if one wishes to skip `Step 1 (Node Finding)` and instead use an existing gene list file generated during a previous run or by other means, then the argument **gene_list_file** must be specified.

Example of content in a gene list file: <br>
**gene_name** <br>
**gene 1**<br>
**gene 2**<br>
**gene 3**<br>
... <br>
**gene N**<br>

#### 3) [netgen_arguments]
`Step 3) (Network Generation)` of the pipeline requires as input a ranked list of edges in order to build an initial network. 

* **edge_score_file**: a dataframe containing one or more scores for the local regulatory interaction models

**Note**: If `Step 2 (Edge Finding)` was run, the argument **edge_score_file** is automatically generated and will be used in `Step 3) (Network Seed Generation)`. However, if one wishes to skip `Step 2 (Edge Finding)` and instead use an existing edge score file generated during a previous run or by other means, then the argument **edge_score_file** must be specified.

#### 4) [netper_arguments]
`Step 4) (Network Finding)` of the pipeline requires as input, a list of allowable nodes, a list of allowable edges, and an initial seed network to perturb.

* **range_operations**: min to max # of node/edge changes allowed per graph, endpoint inclusive
* **numneighbors**: maximum number of perturbed networks to find
* **maxparams**: maximum number of DSGRN parameters to allow in a perturbed network
* **[probabilities]**: subsection containing probabilities of adding or deleting nodes or edges. The values sum to 1.
    * **addNode**: probability to add a node
    * **addEdge**: probability to add an edge
	* **removeNode**: probability to remove a node 
	* **removeEdge**: probability to remove an edge
* **edge_list_file** (optional): a list of edges that are allowed to be added during network finding. In the absence of a file, all edges between nodes will be considered.
* **node_list_file** (optional): a list of nodes that are allowed to be added during network finding. In the absence of a file, nodes with arbitrary names will be added.
* **seed_net_file**:  a network in DSGRN format to be perturbed. May be an empty file.

**Note**: If `Step 3 (Network Generation)` was run, the arguments **edge_list_file**, **node_list_file**, & **seed_net_file** are automatically generated and will be used in `Step 4) (Network Finding)`. However, if one wishes to skip `Step 3 (Network Generation)` and instead use an existing node, edge, and seed network specification files generated in a previous run or by other means, then the arguments **edge_list_file**, **node_list_file**, and **seed_net_file** must be specified.

#### 5) [netquery_arguments]
`Step 5) (Network Queries)` of the pipeline requires as input a list of networks to query through DSGRN.

* **networkfile**:  either a single network in DSGRN format, or a comma separated list of networks in DSGRN format, where the first character of the file is "[" and the last is "]".
* **timeseriesfname**: path to a file or list of files containing the time series data to be pattern matched to network models

**Note**: If `Step 4 (Network Finding)` was run, the argument **networks_file** is automatically generated and will be used in `Step 5 (Network Queries)`. However, if one wishes to skip `Step 4 (Network Finding)` and instead use an existing networks specification file generated in a previous run or by other means, then the argument **networks_file** must be specified. 

**Note**: If `Step 2 (Edge Finding)` was run, the argument **timeseriesfname** is already specified and will be used in `Step 5 (Network Queries)`. However, if one wishes to skip `Step 2 (Edge Finding)`, then the argument **timeseriesfname** must be specified. 


### Optional Arguments

There are many 'optional' arguments for each step of the pipeline which controls the behavior of the algorithm. Although default values are provided for all such arguments if not otherwise specified (See `src/utilities.default_arguments()` for details),  these defaults may need to be adjusted depending on the particulars of the data under investigation.

#### 1) [dlxjtk_arguments]

* **num_reg**: number of random curves for empirical regulation p-value (Default: 1,000,000)
* **num_per**: number of random curves for empirical periodicity p-value (Default: 0, not used in DLxJTK)
* **log_trans**: boolean flag indicating if data requires log transformation (Default: True)
* **pval_combine_method**: a string specifying the method to use for combining p-values of de Lichtenberg's regulation strength and JTK-CYCLE's periodicity measures, respectively, for subsequent use in DLxJTK.
    * Options: max (Default), min, or fisher
* **dlxjtk_cutoff**: an integer specifying the maximum number of nodes to retain in the `gene_list_file` output by DLxJTK (Default: None)

#### 2) [lempy_arguments]

* **loss**: name of the Python function defining the loss function used in the Gibbs posterior (Default: euc_loss)
* **param_bounds**: name of the Python function which returns the parameter bounds for local models (Default: tf_param_bounds)
* **prior**: name of the Python function which returns a dictionary encoding a prior distribution on the space of regulatory models (Default: uniform_prior)
* **normalize**: boolean parameter specifying if time series should be normalized (Default: True)
* **inv_temp**: hyperparameter >= 0 controlling the strength of the update to the prior given the data (Default: 1.0)
* **seed**: number specifying the seed for random number generation (Default: time.time)
* **ground_truth_file**: path to the list of regulation edges which are assumed to be true edges (Used to assess inference accuracy)
* **[[minimizer_params]]**: subsection containing optional parameters for scipy.basinhopping

#### 3) [netgen_arguments]

* **edge_score_column**: the name of the dataframe column from lempy output that contains the score of interest, either "pld" or "norm_loss" (Default: "pld")

Exactly one of the two following arguments must be chosen:
* **edge_score_threshold**: the LEM score cutoff above (">") or below ("<") which edges are accepted for network finding in step 4 (Default: None)
* **num_edges_for_list**: the number of edges from the top scorers from which edges are accepted for network finding in step 4 (Default: None)

Exactly one of the two following arguments must be chosen:
* **seed_threshold**: the LEM score cutoff that takes the largest strongly connected seed network in the top scorers for network finding in step 4 (Default: None)
* **num_edges_for_seed**: the number of edges from the top scorers that will form the seed network for network finding in step 4 (Default: None)

#### 4) [netper_arguments]

* **time_to_wait**: timeout after this time in seconds (Default: 30)
* **random_seed**: random seed for either stochastic or reproducible runs on the same system (Default: system clock)	
* **[[filters]]**: optional subsection containing filters (Default: `{"is_connected" : {}}`). Options:
    * **[[[constrained_inedges]]]**: subsection limiting the number of in-edges to a particular range
        * **min_inedges**: minimum number of in-edges
        * **max_inedges**: maximum number of in-edges 
    * **[[[constrained_outedges]]]**: subsection limiting the number of out-edges to a particular range
        * **min_outedges**: minimum number of out-edges
        * **max_outedges**: maximum number of out-edges 
    * **[[[is_feed_forward]]]**: keeps only networks that are feedforward
        * pass empty dictionary
    * **[[[is_strongly_connected]]]**: keeps only networks that are strongly connected
        * pass empty dictionary
    * **[[[is_connected]]]**: keeps only networks that are weakly connected

**Note:** See the separate `dsgrn_net_gen` documentation for more information on these parameters.

#### 5) [netquery_arguments]

* **domain**: pattern match within the whole domain graph (Default: True)
* **stablefc**: pattern match within all stable full cycles (Default: False)
* **count**: count the number of parameters with the pattern match (True) or shortcut at first match for each network (False) (Default: True)
* **epsilons**: a list of noise level proportions in the interval [0, 0.5] (Default: [0.01, 0.05, 0.10])

**Note:** See the separate `dsgrn_net_query` documentation for more information on these parameters.

### Run Tracking

Copies of the configuration arguments used by any step of the pipeline will be saved in corresponding subfolders of the top-level directory **output_dir** to ensure reproducibility and tracking of results. Also created in the top-level directory **output_dir** is the file `run_manifest.tsv`, which tracks each step executed to this directory and provides an easy means of tracking multiple runs that may have started at different steps.

| run_step | run_input | run_output | 
| :---: | :---: | :---: | 
| dlxjtk | path/to/main/config | path/to/gene_list_file_1 | 
| lempy  | path/to/gene_list_file_1 | path/to/edge_score_file_1 |    
| netgen | path/to/edge_score_file_1 | path/to/seed_net_file_1 |
| netgen | path/to/edge_score_file_1  | path/to/seed_net_file_2  | 
| netper | path/to/seed_net_file_2 | path/to/networks_file |
| netquery | path/to/networks_file | path/to/queryresults |

In the above example, a single run of `Step 1)` was used to generate a gene list which was passed to a single run of `Step 2`, whose outpute was then used in two runs of `Step 3` to generate two different seed networks, the second of which was subsequently passed through the remainder of the pipeline. In this way, `run_manifest.tsv` can be used to quickly track paths through branching runs executing from different steps with possibly different configurations.


### Data Inputs/Outputs
The goal of the pipeline is to identify the core transcriptional regulatory elements involved in producing a program of periodic gene expression and it relies on mutiple biological/experimental data types to achieve this goal.

#### data_file
Time series gene expression data is required by each step of the pipeline because the pipeline leverages the dynamics of gene expression to identify regulatory elements and their interactions. This data should be given as a single tab-delimited text file containing time series gene expression profiles, with rows indexed by gene names and columns indexed by time points. 

|  | time 1 | time 2 | ... | time n |
| :---: | :---: | :---: | :---: | :---: |
| **gene 1** | 653.13 | 578.55 | ... | 437.58 |
| **gene 2** | 226.35 | 254.71 | ... | 345.22 |
| **gene 3** | 474.19 | 489.25 | ... | 611.81 |
| ... | ... | ... | ... | ... |
| **gene N** | 1274.12 | 1124.77 | ... | 929.48 |

#### annotation_file
Given the goal to identify the transcriptional regulatory elements, we can refine the search by including prior knowledge/evidence by flagging genes according to their potential function as transcriptional regulators. This is handled by including a tab-delimited file with rows indexed by genes and columns labelled by allowable models of local regulation (See LEM for details: https://doi.org/10.1186/s13059-016-1076-z, https://gitlab.com/biochron/lempy).

|  | tf_act | tf_rep | target |
| :---: | :---: | :---: | :---: |
| **gene 1** |0 | 0 | 1 |
| **gene 2** | 1 | 1 | 1 |
| **gene 3** | 0 | 1 | 1 |
| ... | ... | ... | ... |
| **gene N** | 0 | 0 | 0 |

In the above example, **gene 1** codes for a protein that is known to not function as a transcription factor (TF), perhaps there is evidence that **gene 2** can act as a TF (e.g. has a protein binding domain) but it is not known if it is a repressor or an activator, and **gene 3** is believed to be a repressor only. Genes 1,2 and 3 are also allowed to be targets, but gene N is not. Thus, gene N will be excluded from consideration.  

It is not required that the gene annotation file include the same list of genes as appears in the time series data file. In fact, the annotation file can include only those genes which are allowed to be either a target or a regulator. Any genes not explicitly appearing in the annotation file are assumed to not be a target or a regulator no matter how well they score by the DLxJTK algorithm and will thus not appear in **gene_list_file** output by `Step 1) (Node Finding)` or **edge_score_file** output by `Step 2) (Edge Finding)`.

Supplying an annotation file is also not required, however, `Step 2 (Edge Finding)` does require an annotation file. Therefore, if an annotation file is not supplied, one will be made and saved into the directory specified in **output_dir**. This new annotation file will contain a unique set of genes across all time series datasets supplied. Additionally, each gene will be annotated as a transcription factor as well as a target.

#### gene_list_file
`Step 2 (Edge Finding)` requires also a text file containing a list of allowable genes.  Local regulatory interactions will be determined for all genes appearing as transcriptional targets or regulators in the annotation file provided that those genes also appear in the **gene_list_file**.

#### edge_score_file
`Step 3 (Network Generation)` requires a dataframe containing a list of local regulatory models scored by some measure. Thus the **edge_score_file** will be structured as

| model | score_1 | score_2 | 
| :---: | :---: | :---: |
| **gene_1=tf_act(gene_1)** | .99 | 105 |
| **gene_2=tf_rep(gene_16)** | .95 | 106 |
| ... | ... | ... | 
| **gene_i=tf_rep(gene_j)** | .01 | 13 | 

The entries listed in the column `models` must be of the form `gene_i=tf_act(gene_j)` or `gene_i=tf_rep(gene_j)`, encoding respectively activation and repression of `gene_i` `by gene_j`. The edge score file may contain multiple columns scoring edges according to different measures. The list of models should be able to be ranked according to each score. 

#### edge_list_file
`Step 4 (Network Finding)` can optionally take a text file listing edges that may be added during network finding, where each line is of the form `gene1=reg(gene2)`, where reg is `a` for activation and `r` for repression. 

**Note:** Any self-repressing edges included in the edge list will not be used to construct networks, because this topology is not currently supported by DSGRN. Also, no networks will be constructed that have multi-edges; i.e. there can be no simultaneous direct paths for both activation and repression. Mixed effects can only happen when at least one path from source to target is indirect.


#### node_list_file
`Step 4 (Network Finding)` can optionally take a text file listing gene names that may be added as nodes during network finding, where each line has a single gene name. 

#### seed_net_file
`Step 4 (Network Finding)` requires a text file with a DSGRN formatted network from which to start perturbations (see DSGRN documentation). This file may be empty. Example:
```bash
X : X+Y
Y : X
```

#### networks_file 
`Step 5 (Network Queries)` requires a text file that either contains a single network in DSGRN format, or a comma separated list of networks in DSGRN format, where the first character of the file is "[" and the last is "]". Example where the first network is the same as above and `\n` denotes a new line:
```bash
["X : X+Y\nY : X", "X : Z\nY : ~X\nZ : Y"]
```
Additional spaces in the file format are acceptable. For example:
```bash
["X : X + Y \n Y : X", "X : Z \n Y : ~X \n Z : Y"]
```
See the separate `DSGRN` documentation for more information on this file format.

## Troubleshooting

1) First make sure that the conda environment `dat2net` is activated.
2) A common cause of mpiexec errors is specifying too many cores. Try reducing `num_proc`.
3) If few or no networks are produced during the network finding step, see the `Troubleshooting` section in the `README.md` in the `dsgrn_net_gen` folder for possible causes.
4) Clusters are often problematic. 

    (a) Make sure `dat2net` is activated before calling the scheduler.
    
    (b) Make sure that the `num_proc` parameter, the number of cpus, in the pipeline configuration file is the same number as requested through the cluster scheduler.
    
    (c) Make sure to load the system `openmpi` module. 
    
    (d) Example SLURM configuration file to be called with `sbatch`: 
    
    ```bash
    #!/bin/bash -l
    #SBATCH -o pipeline.out
    #SBATCH -e pipeline.err
    #SBATCH -n 16                 # number tasks (should equal num_proc)
    #SBATCH -N 1                  # number nodes
    #SBATCH -p common             # partition
    
    module load OpenMPI/4.0.1
    time python src/dat2net.py pipeline_config.txt
    ```
   Setting the number of nodes `-N` to 1 is for performance purposes, but is not necessary.















