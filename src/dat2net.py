import os, ast, shutil
import json
import datetime
import pandas as pd
import utilities as utls
from configobj import ConfigObj
import lem_network_guesser as lng
from dsgrn_net_gen.makejobs import Job


if __name__ == '__main__':
    """
    initialize a pipeline run with configuration file, setting defaults as necessary, and overwriting configuration 
    file values with optional command line arguments 
    """

    datetimestr = datetime.datetime.now().strftime('%Y%m%d%H%M%S')

    import argparse

    parser = argparse.ArgumentParser(prog='Data-to-Networks Pipeline',
                                     description='')

    # - required command-line argument -
    parser.add_argument('config_file',
                        action='store',
                        help='full path to configuration file')
    parser.add_argument('--verbose',
                        action='store',
                        help='verbose terminal output flag (default: True)')
    parser.add_argument('--num_proc',
                        action='store',
                        type=int,
                        help='integer specifying the number of MPI processes to use')
    args = parser.parse_args()

    # Load and parse master config file
    config_file = args.config_file
    config_file_name = os.path.basename(config_file)

    # parse config file
    def parse_co(section,key):
        try:
            results = ast.literal_eval(section[key])
        except:
            results = section[key]
        return results

    co = ConfigObj(ConfigObj(config_file).walk(parse_co))
    co['config_file'] = config_file
    os.makedirs(co['output_dir'],exist_ok=True)

    # This makes a directory in the output_dir called 'input_files'
    input_dir_path = os.path.join(co['output_dir'], 'input_files')
    os.makedirs(input_dir_path, exist_ok=True)
    # This makes a directory for saving datatime stamped executed config files 
    exec_config_path = os.path.join(co['output_dir'], 'input_files', 'exec_configs')
    os.makedirs(exec_config_path, exist_ok=True)
    # This makes a directory for saving datatime stamped run manifests 
    dts_mani_path = os.path.join(co['output_dir'], 'input_files', 'dts_manifests')
    os.makedirs(dts_mani_path, exist_ok=True)

    # Overwrite executed/saved config file based on optional command-line inputs
    if args.verbose is not None:
        co['verbose'] = utls.str_to_bool(args.verbose)
    else:
        if co.get('verbose') is None:
            co['verbose'] = True
    if args.num_proc:
        co['num_proc'] = args.num_proc

    sections_to_run = co.sections[:]

    # --------------- Step 0a: Run Tracking ------------------
    # check if the output directory and run manifest file exist, otherwise create
    if os.path.isfile(os.path.join(co['output_dir'], 'run_manifest.tsv')):
        manifest_df = pd.read_csv(os.path.join(co['output_dir'], 'run_manifest.tsv'), sep='\t', comment='#')
    else:
        if not os.path.isdir(co['output_dir']):
            os.makedirs(co['output_dir'])
        manifest_df = pd.DataFrame(columns=['run_step', 'run_input', 'run_output'])

    # --------------- Step 0b: Annotation File Check --------
    # Check if there is an annotation file specified in the config file.
    # If there is not, then create one where all genes are annotated as transcription factors and targets
    if co.get('annotation_file') is None:
        # make the annotation file and save to the output directory.
        annot_path = utls.make_annot_file(co.get('data_file'), co.get('output_dir'))
        # update config to contain 'annotation_file' field containing the path to the new annotation file
        co['annotation_file'] = annot_path

    # --------------- Step 0c: Check if IDVisualizer Connection wanted --------
    # copies user-specified files in the input_files directory for easier connection to the IDVisualizer
    if 'IDVconnection' in co.keys():
        if co.get('IDVconnection') is not None:
            if co['IDVconnection']:
                utls.make_inputs_directory(co)
        elif co.get('IDVconnection') is None:
            co['IDVconnection'] = False
    else:
        co['IDVconnection'] = False
        
    # save datatime stamped executed config file
    shutil.copyfile(config_file, os.path.join(exec_config_path,"dat2net_config_{}.txt".format(datetimestr)))

    # --------------- Step 1: Node Finding ------------------
    if 'dlxjtk_arguments' in sections_to_run:
        if co['verbose']:
            print('\n     STEP 1: Node Finding')

        # Generate DLxJTK config file arguments
        co['dlxjtk_datetime'] = datetimestr
        dlxjtk_config = utls.gen_dlxjtk_config(co)

        # Compute DL regulator p-value
        if co['verbose']:
            print('\n  Computing DL regulator p-values')
            print('  -----------------------------')

        dl_rep_files_dict = {}
        # Run pyDL for single dataset
        if len(dlxjtk_config['data_file']) == 1:
            os.system('mpiexec -n %s python pydl/pydl.py %s -T %s -o %s -r %s -p %s -l %s -v %s' % (dlxjtk_config['num_proc'],
                                                                                                                    dlxjtk_config['data_file'][0],
                                                                                                                    dlxjtk_config['periods'],
                                                                                                                    dlxjtk_config['dl_output_file'],
                                                                                                                    dlxjtk_config['num_reg'],
                                                                                                                    dlxjtk_config['num_per'],
                                                                                                                    dlxjtk_config['log_trans'],
                                                                                                                    dlxjtk_config['verbose']))
        else:
            # Run pyDL for multiple datasets and combine results
            dl_rep_files_dict = {}
            for rep, (data_file, pers) in enumerate(zip(dlxjtk_config['data_file'], dlxjtk_config['periods'])):
                rep_dl_file = f'{dlxjtk_config["dl_output_file"].split(".")[0]}_rep{rep+1}.tsv'
                dl_rep_files_dict[rep_dl_file] = f'rep{rep+1}'
                os.system('mpiexec -n %s python pydl/pydl.py %s -T %s -o %s -r %s -p %s -l %s -v %s' % (dlxjtk_config['num_proc'],
                                                                                                        data_file,
                                                                                                        pers,
                                                                                                        rep_dl_file,
                                                                                                        dlxjtk_config['num_reg'],
                                                                                                        dlxjtk_config['num_per'],
                                                                                                        dlxjtk_config['log_trans'],
                                                                                                        dlxjtk_config['verbose']))

            # Combine pyDL Results of replicates
            utls.combine_pvals_results(dl_rep_files_dict, dlxjtk_config, 'dl')

        # Compute JTK periodicity p-value
        if co['verbose']:
            print('\n  Computing JTK periodicity p-values')
            print('  -------------------------------')

        if len(dlxjtk_config['data_file']) == 1:
            os.system('python pyjtk/pyjtk.py %s -T %s -o %s' % (dlxjtk_config['data_file'][0],
                                                                dlxjtk_config['periods'],
                                                                dlxjtk_config['jtk_output_file']))
        else:
            jtk_rep_files_dict = {}
            for rep, (data_file, pers) in enumerate(zip(dlxjtk_config['data_file'], dlxjtk_config['periods'])):
                rep_jtk_file = f'{dlxjtk_config["jtk_output_file"].split(".")[0]}_rep{rep + 1}.tsv'
                jtk_rep_files_dict[rep_jtk_file] = f'rep{rep + 1}'
                os.system('python pyjtk/pyjtk.py %s -T %s -o %s' % (data_file,
                                                                    pers,
                                                                    rep_jtk_file))
            # Combine pyJTK Results of replicates
            utls.combine_pvals_results(jtk_rep_files_dict, dlxjtk_config, 'jtk')

        # Combine DL and JTK p-values into the DLxJTK score
        if co['verbose']:
            print('\n  Computing DLxJTK scores')
            print('  -------------------------------')
        if len(dlxjtk_config['data_file']) == 1:
            os.system('python src/dlxjtk.py %s %s %s %s %s %s' % (dlxjtk_config['dl_output_file'],
                                                               dlxjtk_config['jtk_output_file'],
                                                               dlxjtk_config['dlxjtk_output_file'],
                                                               dlxjtk_config['gene_list_file'],
                                                               dlxjtk_config['dlxjtk_cutoff'],
                                                               dlxjtk_config['annotation_file']))
        else:
            # add --pval_combine_col and either 'max' or 'fisher' to change p-value combination from "min"
            os.system('python src/dlxjtk.py %s %s %s %s %s %s --rep_numb %i --pval_combine_col %s' % (dlxjtk_config['dl_output_file'],
                                                                                                      dlxjtk_config['jtk_output_file'],
                                                                                                      dlxjtk_config['dlxjtk_output_file'],
                                                                                                      dlxjtk_config['gene_list_file'],
                                                                                                      dlxjtk_config['dlxjtk_cutoff'],
                                                                                                      dlxjtk_config['annotation_file'],
                                                                                                      len(dlxjtk_config['data_file']),
                                                                                                      dlxjtk_config['pval_combine_method']))

        # Save a copy of the DLxJTK config file to track provenance
        dlxjtk_config['periods'] = utls.revert_periods_format(dlxjtk_config['periods'])
        dlxjtk_config.write()

        # Add the file specifying the gene list based on DLxJTK results
        # to be passed to LEMpy edge scoring
        if 'lempy_arguments' in sections_to_run:
            if not co.get('lempy_arguments'):
                co['lempy_arguments'] = dict()
            co['lempy_arguments']['gene_list_file'] = dlxjtk_config['gene_list_file']

        # Update and save run manifest
        manifest_df = manifest_df.append({'run_step': 'dlxjtk',
                                          'run_input': dlxjtk_config['data_file'],
                                          'run_output': os.path.split(os.path.abspath(dlxjtk_config['gene_list_file'] ))[0]},
                                         ignore_index=True)
        with open(os.path.join(co['output_dir'], 'run_manifest.tsv'), 'w') as f:
            f.write('# Run Manifest for %s\n' % co['output_dir'])
            manifest_df.to_csv(f, sep='\t', index=False)


    # --------------- Step 2: Edge Finding -------------------
    if 'lempy_arguments' in sections_to_run:
        if co['verbose']:
            print('\n    STEP 2: Edge Finding')

        # Generate DLxJTK config file arguments
        co['lempy_datetime'] = datetimestr
        lempy_config = utls.gen_lempy_config(co)

        # Save a copy of the LEMpy config file to track provenance and execute LEMpy
        os.makedirs(os.path.split(lempy_config.filename)[0])
        lempy_config.write()

        # Run LEMpy from the temporary config file
        os.system('mpiexec -n %s python lempy/lempy.py %s' % (co['num_proc'],
                                                              lempy_config.filename))

        # Add the file specifying the ranked edge list based on LEM results
        # to be passed to the seed network generation
        net_gen_in = os.path.join(lempy_config['output_dir'], 'summaries')
        scorefiles = []
        for d in os.listdir(net_gen_in):
            if d.startswith("ts"):
                scorefiles.append(os.path.join(net_gen_in, '{}/allscores_{}.tsv'.format(d, d)))
        lempy_config['edge_score_file'] = scorefiles
        if 'netgen_arguments' in sections_to_run:
            if not co.get('netgen_arguments'):
                co['netgen_arguments'] = dict()
            co['netgen_arguments']['edge_score_file'] = lempy_config['edge_score_file']

        # Update and save run manifest
        manifest_df = manifest_df.append({'run_step': 'lempy',
                                          'run_input': os.path.split(os.path.abspath(lempy_config['gene_list_file']))[0],
                                          'run_output': os.path.abspath(net_gen_in)},
                                         ignore_index=True)
        with open(os.path.join(co['output_dir'], 'run_manifest.tsv'), 'w') as f:
            f.write('# Run Manifest for %s\n' % co['output_dir'])
            manifest_df.to_csv(f, sep='\t', index=False)


    # --------------- Step 3: Seed network -------------------
    if 'netgen_arguments' in sections_to_run:
        if co['verbose']:
            print('\n    STEP 3: Seed Network')

        # make network_finding folder
        co['netgen_datetime'] = datetimestr
        netgen_config = utls.gen_netgen_config(co)

        if not os.path.isdir(netgen_config['output_dir']):
            os.mkdir(netgen_config['output_dir'])

        # execute a seed network
        netgen_config['node_list_file'], netgen_config['edge_list_file'], netgen_config['seed_net_file']=\
            lng.generate_lem_networks(netgen_config['edge_score_file'],
                              netgen_config['edge_score_column'],
                              netgen_config['output_dir'],
                              edge_threshold=netgen_config['edge_score_threshold'],
                              num_edges_for_list=netgen_config['num_edges_for_list'],
                              num_top_edges=netgen_config['num_edges_for_seed'],
                              seed_threshold=netgen_config['seed_threshold'],
                              comment="#")

        # Save a copy of the netgen config file to track provenance
        netgen_config.write()

        # Add the files specifying the ranked edge list, node list, and the seed network file
        # to be passed to network perturbations
        if 'netper_arguments' in sections_to_run:
            if not co.get('netper_arguments'):
                co['netper_arguments'] = dict()
            co['netper_arguments']['edgefile'] = netgen_config['edge_list_file']
            co['netper_arguments']['nodefile'] = netgen_config['node_list_file']
            co['netper_arguments']['networkfile'] = netgen_config['seed_net_file']

        # Update and save run manifest
        manifest_df = manifest_df.append({'run_step': 'netgen',
                                          'run_input': os.path.split(os.path.split(os.path.abspath(netgen_config['edge_score_file'][0]))[0])[0],
                                          'run_output': os.path.split(os.path.abspath(netgen_config['edge_list_file']))[0]},
                                         ignore_index=True)
        with open(os.path.join(co['output_dir'], 'run_manifest.tsv'), 'w') as f:
            f.write('# Run Manifest for %s\n' % co['output_dir'])
            manifest_df.to_csv(f, sep='\t', index=False)


    # --------------- Step 4: Perturbations ------------------
    if 'netper_arguments' in sections_to_run:
        if co['verbose']:
            print('\n    Step 4: Network Finding')

        co['netper_datetime'] = datetimestr
        netper_config = utls.gen_netper_config(co)

        # save temporary json format configuration file for input to network perturbations
        if not os.path.isdir(netper_config['resultsdir']) and netper_config['resultsdir']:
            os.makedirs(netper_config['resultsdir'])
        netper_json_config_file = os.path.join(netper_config['resultsdir'], 'netper_config.json')
        json.dump(netper_config, open(netper_json_config_file, 'w'))

        job = Job(netper_json_config_file)
        job.run()

        # Add the files specifying the networks produced by perturbations to be passed to DSGRN queries
        netper_networks_file = os.path.join(netper_config['resultsdir'], 'dsgrn_net_gen_results' + co['netper_datetime'], 'networks' + co['netper_datetime'], 'networks.txt')
        if 'netquery_arguments' in sections_to_run:
            if not co.get('netquery_arguments'):
                co['netquery_arguments'] = dict()
            co['netquery_arguments']['networkfile'] = netper_networks_file

        # delete temporary json format configuration file for input to network perturbations
        os.remove(os.path.join(netper_config['resultsdir'], 'netper_config.json'))

        # Update and save run manifest
        manifest_df = manifest_df.append({'run_step': 'netper',
                                          'run_input': os.path.split(os.path.abspath(netper_config['edgefile']))[0],
                                          'run_output': os.path.split(os.path.abspath(netper_networks_file))[0]},
                                         ignore_index=True)
        with open(os.path.join(co['output_dir'], 'run_manifest.tsv'), 'w') as f:
            f.write('# Run Manifest for %s\n' % co['output_dir'])
            manifest_df.to_csv(f, sep='\t', index=False)


    # --------------- Step 5: DSGRN Queries ------------------
    if 'netquery_arguments' in sections_to_run:
        if co['verbose']:
            print('\n    Step 5: DSGRN Queries', flush=True)

        co['netquery_datetime'] = datetimestr
        netquery_config = utls.gen_netquery_config(co)

        # save temporary json format configuration file for input to network queries
        if not os.path.isdir(netquery_config['resultsdir']):
            os.makedirs(netquery_config['resultsdir'])
        netquery_json_config_file = os.path.join(netquery_config['resultsdir'], 'netquery_config.json')
        json.dump(netquery_config, open(netquery_json_config_file, 'w'))

        # command = " ".join(["mpiexec", "-n", str(co['num_proc']), "python", "dsgrn_net_query/src/dsgrn_net_query/queries/CountPatternMatch.py", co['netquery_arguments']['networkfile'], netquery_json_config_file, netquery_config['resultsdir'], ">{}".format(os.path.join(netquery_config['resultsdir'],"dsgrn_net_query.log")), "2>&1"])

        command = " ".join(["mpiexec", "-n", str(co['num_proc']), "python", "dsgrn_net_query/src/dsgrn_net_query/queries/CountPatternMatch.py", co['netquery_arguments']['networkfile'], netquery_json_config_file, netquery_config['resultsdir']])

        os.system(command)

        # delete temporary json format configuration file for input to network perturbations
        os.remove(os.path.join(netquery_config['resultsdir'], 'netquery_config.json'))

        # Update and save run manifest
        manifest_df = manifest_df.append({'run_step': 'netquery',
                                          'run_input': os.path.split(os.path.abspath(netquery_config['networkfile']))[0],
                                          'run_output': os.path.abspath(os.path.join(netquery_config['resultsdir'], 'queries' + co['netquery_datetime']))},
                                         ignore_index=True)
        if co['verbose']:
            print("Queries complete.")

        with open(os.path.join(co['output_dir'], 'run_manifest.tsv'), 'w') as f:
            f.write('# Run Manifest for %s\n' % co['output_dir'])
            manifest_df.to_csv(f, sep='\t', index=False)

    # Makes a run_manifest with a datetime stamp in the name. save to input_files/dts_manifests
    with open(os.path.join(dts_mani_path, 'run_manifest_{}.tsv'.format(datetimestr)), 'w') as f:
        f.write('# Run Manifest for %s\n' % co['output_dir'])
        manifest_df.to_csv(f, sep='\t', index=False)
