import dsgrn_utilities.graphtranslation as gt
import pandas as pd
import os


def generate_lem_networks(scorefiles, column, outputdir, num_top_edges=None, seed_threshold=None, num_edges_for_list=None, edge_threshold=None,
                          comment="#"):
    '''
    Extract a set of top edges from the LEM file using either a number of edges or a LEM score threshold. Two arguments must be specified
    -- either a number or a threshold for a seed network (for random network perturbations using dsgrn_net_gen) and either a number or a
    threshold for the top edges to add during perturbations. The seed network is represented in DSGRN network specification format and
    the edges for perturbations are written as a list. Every node that participates in at least one edge in the edge list is written to a .txt
    file.

    Self-repressing edges are removed from the LEM scoring file because they are not supported by DSGRN. Similarly, multi-edges are removed
    from the seed network, but not from the edge list for perturbations. Multi-edges in the edge list are handled by the perturbation
    algorithm in dsgrn_net_gen.

    :param scorefiles: list of file names with lem scores; full path required if not in local folder
                        if input type is string, it will be interpreted as a single file
    :param column: name of score column in file to use for network finding, "pld" or "norm_loss"
    :param outputdir: location for saving files

    Required: Choose exactly one of the two following keyword arguments:
    :param num_top_edges: number of edges to put into the seed network; used only with "choose_top_edges" function, default = None
    :param seed_threshold: float indicating cut-off score or threshold for choosing the number of edges in a seed network; used with "filter_by_threshold", default = None

    Required: Choose exactly one of the two following keyword arguments:
    :param num_edges_for_list: number of edges to put into the allowable edge list; used only with "choose_top_edges" function, default = None
    :param edge_threshold: float indicating cut-off score or threshold for forming the list of allowable edges; used with "filter_by_threshold", default = None

    Optional:
    :param comment: comment character in file, default ="#"

    :return: file names or a network string in DSGRN format
    '''
    #sanity check
    if column not in ["pld","norm_loss"]:
        raise ValueError("The input argument 'column' must be either 'pld' or 'norm_loss'.\nIf those output column names are missing from lempy output, this seed network generator will fail.")
    # assign order
    order = ">" if column=="pld" else "<"
    # read all score files
    all_sources, all_targets, all_regs, all_scores = parse_edge_file(scorefiles, column, comment)
    # creating a dictionary will allow a set-like operation to be done over multiple LEM score files
    best_scores = {}
    # get best scores for edges across all score files
    for srcs,tgts,tregs,scrs in zip(all_sources,all_targets,all_regs,all_scores):
        edges = choose_top_edges(scrs,srcs,tgts,tregs, len(srcs)+1)
        best_scores = rank_best(edges,best_scores.copy(),order)

    # drop self-repressors since they are not currently supported by DSGRN (it will run, but it's not correct except in a develop branch)
    best_scores = remove_self_repressors(best_scores.copy())

    # build seed network
    # drop multiedges so that the seed network is a graph instead of a multigraph (not computable by DSGRN)
    no_multi_scores = remove_multiedges(best_scores,order)
    ordered_scores, ordered_sources,ordered_targets,ordered_regs = order_edges(no_multi_scores,order)
    if seed_threshold is not None and num_top_edges is None:
        top_seed_edges = filter_by_threshold(ordered_scores,ordered_sources,ordered_targets,ordered_regs,order,seed_threshold)
    elif num_top_edges is not None and seed_threshold is None:
        top_seed_edges = choose_top_edges(ordered_scores,ordered_sources,ordered_targets,ordered_regs,num_top_edges)
    else:
        raise ValueError("Exactly one of 'num_top_edges' or 'seed_threshold' must be specified.")
    if top_seed_edges:
        seed_network = get_DSGRN_spec(*list(zip(*top_seed_edges.keys())))
    else:
        seed_network = ""
    seedfile = save_seed(seed_network,outputdir)

    # get node and edge lists
    # drop seed network edges first
    for e in top_seed_edges:
        best_scores.pop(e)
    # order best scores
    ordered_scores, ordered_sources,ordered_targets,ordered_regs = order_edges(best_scores,order)
    if edge_threshold is not None and num_edges_for_list is None:
        top_edges = filter_by_threshold(ordered_scores,ordered_sources,ordered_targets,ordered_regs,order,edge_threshold)
    elif num_edges_for_list is not None and edge_threshold is None:
        top_edges = choose_top_edges(ordered_scores,ordered_sources,ordered_targets,ordered_regs,num_edges_for_list)
    else:
        raise ValueError("Exactly one of 'edge_threshold' or 'num_edges_for_list' must be specified.")
    if top_edges:
        nodefile, edgefile = save_files(*list(zip(*top_edges.keys())),outputdir)
    else:
        nodefile, edgefile = save_files([],[],[],outputdir)

    return nodefile, edgefile, seedfile


def rank_best(top_edges,best_scores,order):
    for t in top_edges:
        if t not in best_scores:
            best_scores[t] = top_edges[t]
        else:
            if order == ">":
                best_scores[t] = max(best_scores[t], top_edges[t])
            elif order == "<":
                best_scores[t] = min(best_scores[t], top_edges[t])
    return best_scores


def remove_multiedges(score_dict,order):
    source_target = {}
    for edge,score in score_dict.items():
        st = edge[:2]
        if st not in source_target:
            source_target[st] = (edge,score)
        if st in source_target:
            if order == "<" and score < source_target[st][1]:
                source_target[st] = (edge,score)
            elif order == ">" and score > source_target[st][1]:
                source_target[st] = (edge, score)
    return dict(source_target.values())


def remove_self_repressors(score_dict):
    remove = []
    for e in score_dict:
        if e[0] == e[1] and e[2] == "r":
            remove.append(e)
    for e in remove:
        score_dict.pop(e)
    return score_dict


def choose_top_edges(scores,source,target,type_reg,num):
    edges = {t: scores[k] for k, t in enumerate(zip(source[:num], target[:num], type_reg[:num]))}
    return edges


def filter_by_threshold(scores,source,target,type_reg,order,threshold):
    if order == "<":
        try:
            ind = next(k for k, l in enumerate(scores) if l > threshold)
        except StopIteration:
            ind = len(scores)
    elif order == ">":
        try:
            ind = next(k for k, l in enumerate(scores) if l < threshold)
        except StopIteration:
            ind = len(scores)
    else:
        raise ValueError("Second element of input argument 'column' must be '<' or '>'.")
    return choose_top_edges(scores,source,target,type_reg,ind)


def get_DSGRN_spec(source,target,type_reg):
    graph = makegraph(source,target,type_reg)
    return gt.createEssentialNetworkSpecFromGraph(graph)


def makegraph(source,target,type_reg):
    # make gt.Graph
    edges = list(zip(source,target,type_reg))
    genes = sorted(set(source).union(target))
    graph = gt.Graph()
    for k,g in enumerate(genes):
        graph.add_vertex(k,label=g)
    for s, t, tr in edges:
        graph.add_edge(genes.index(s),genes.index(t),label=tr)
    return graph


def order_edges(best_scores,order):
    edges,scores = zip(*list(best_scores.items()))
    sources,targets,regs = zip(*edges)
    if order == "<":
        [scores, source, target, type_reg] = sort_by_list(scores,[sources, targets, regs], reverse=False)
    elif order == ">":
        [scores, source, target, type_reg] = sort_by_list(scores, [sources, targets, regs], reverse=True)
    else:
        raise ValueError("Second element of input argument 'column' must be '<' or '>'.")
    return scores, source, target, type_reg


def save_files(source,target,type_reg,outputdir):
    nodefile = os.path.join(outputdir, "node_file.txt")
    edgefile = os.path.join(outputdir, "edge_file.txt")
    with open(nodefile,"w") as f:
        f.write("\n".join(set(source).union(target)))
    with open(edgefile,"w") as f:
        f.write("\n".join(["{}={}({})".format(*e) for e in zip(target,type_reg,source)]))
    return nodefile,edgefile


def save_seed(networks,outputdir):
    networkfile = os.path.join(outputdir,"seed_network_file.txt")
    with open(networkfile,"w") as f:
        f.write(str(networks))
    return networkfile


def sort_by_list(X, Y, reverse=False):
    # X is a list of length n, Y is a list of lists of length n
    # sort every list in Y by either ascending order (reverse = False) or descending order (reverse=True) of X
    newlists = [[] for _ in range(len(Y) + 1)]
    for ztup in sorted(zip(X, *Y), reverse=reverse):
        for k, z in enumerate(ztup):
            newlists[k].append(z)
    return newlists


def parse_edge_file(fnames, column, comment="#"):
    '''
    Parses lem score file of the following format:
    Any number of comment lines denoted starting with comment character(s)
    Columns delimited by delimiter character(s), or inferred by file type
    File must have the following column:
    model:  TARGET_GENE = TYPE_REG(SOURCE_GENE)
            Activating TYPE_REG must have an "a" or "A" in the string, and no "r" or "R"
            Repressing TYPE_REG must have an "r" or "R" in the string, and no "a" or "A"
    :param fnames: list of file names with lem scores; full path required if not in local folder
                    if it's of type string, it will be interpreted as a single file name
    :param column: score column name
    :param comment: comment character in file, usually "#"
    :return: four lists containing the sources, targets, types of regulation, and scores
    '''
    if column == "pld" and not isinstance(fnames,str):
        #for multiple input time series, use only the last one to ensure that the Bayesian updates for LEM have been modified by previous datasets
        fnames = [sorted(fnames)[-1]]
    elif isinstance(fnames,str):
        fnames = [fnames]
    sources,targets,type_regs,scores = [],[],[],[]
    for fname in fnames:
        ext = fname.split(".")[-1]
        if ext == "tsv":
            df = pd.read_csv(fname,delim_whitespace=True,comment=comment)
        elif ext == "csv":
            df = pd.read_csv(fname,comment=comment)
        else:
            raise ValueError("Extension .{} not recognized. Please specify delimiter.".format(ext))
        models = list(df["model"].values)
        scores.append(list(df[column].values))
        source=[]
        type_reg=[]
        target=[]
        for m in models:
            first = m.split("=")
            target.append(first[0])
            second = first[1].split("(")
            reg = second[0]
            if ("a" in reg or "A" in reg) and not("r" in reg or "R" in reg):
                type_reg.append("a")
            elif ("r" in reg or "R" in reg) and not("a" in reg or "A" in reg):
                type_reg.append("r")
            else:
                raise ValueError("Regulation type is ambiguous. Regulation must be a string with 'a' and no 'r' for activation and 'r' with no 'a' for repression.")
            source.append(second[1][:-1])
        sources.append(source)
        targets.append(target)
        type_regs.append(type_reg)
    return sources, targets, type_regs, scores

