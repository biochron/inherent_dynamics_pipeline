import os, argparse
import pandas as pd
import numpy as np
import datetime


def dlxjtk_func(row):
    # computes DLxJTK score for one gene in df
    amp = row["dl_reg_pval_norm"]
    per = row["jtk_per_pval_norm"]
    return per * amp * (1 + ((per / 0.001) ** 2)) * (1 + ((amp / 0.001) ** 2))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='DLxJTK function',
                                     description='computes DLxJTK score from DL and JTK p-values to rank transcriptional regulatory elements')
    parser.add_argument('pydl_file',
                        action='store',
                        help='output results of pyDL run')
    parser.add_argument('pyjtk_file',
                        action='store',
                        help='output results of pyJTK run')
    parser.add_argument('dlxjtk_file',
                        action='store',
                        help='output results of combined DLxJTK')
    parser.add_argument('gene_list_file',
                        action='store',
                        help='path to file to save gene list of allowable genes with high DLxJTK score')
    parser.add_argument('dlxjtk_cutoff',
                        action='store',
                        help='integer cutoff specifying number of genes to keep')
    parser.add_argument('annotation_file',
                        action='store',
                        help='path to annotation file specifying the allowed regulatory behavior of genes')
    parser.add_argument('--rep_numb',
                        action='store',
                        help='The number of replicates. Default is 1.',
                        type=int,
                        default=1,
                        required=False)
    parser.add_argument('--pval_combine_col',
                        action='store',
                        help='The p-value combination to use if there are 2 or more replicates. Can be "min", "max", or "fisher". Default is max p-value.',
                        type=str,
                        default='max',
                        required=False)

    args = parser.parse_args()

    # parse file names and locations to create output files
    pydl_file = args.pydl_file
    pyjtk_file = args.pyjtk_file
    dlxjtk_file = args.dlxjtk_file
    gene_list_file = args.gene_list_file
    dlxjtk_cutoff = args.dlxjtk_cutoff
    annotation_file = args.annotation_file
    rep_numb = args.rep_numb
    pval_combine_col = args.pval_combine_col

    # load dl and jtk results into dataframes
    dl_df = pd.read_csv(pydl_file, delimiter='\t', index_col=0, comment='#')
    jtk_df = pd.read_csv(pyjtk_file, delimiter='\t', index_col=0, comment='#')

    if rep_numb == 1:

        dl_df.rename(columns={'p_reg': 'dl_reg_pval', 'p_reg_norm': 'dl_reg_pval_norm'}, inplace=True)
        jtk_df.rename(columns={'p-value': 'jtk_per_pval'}, inplace=True)

    elif rep_numb > 1:

        dl_df.rename(columns={f'p_reg_{pval_combine_col}': 'dl_reg_pval', f'p_reg_{pval_combine_col}_norm': 'dl_reg_pval_norm'}, inplace=True)
        jtk_df.rename(columns={f'p-value_{pval_combine_col}': f'jtk_per_pval'}, inplace=True)

    # normalize jtk p-values for use in dlxjtk score
    jtk_df['jtk_per_pval_norm'] = jtk_df['jtk_per_pval'] / np.median(jtk_df['jtk_per_pval'])

    # merge dl and jtk dataframes
    dlxjtk_df = pd.merge(dl_df, jtk_df, left_index=True, right_index=True)
    dlxjtk_df = dlxjtk_df[['dl_reg_pval', 'dl_reg_pval_norm', 'jtk_per_pval', 'jtk_per_pval_norm']]

    # compute dlxjtk score and sort genes by the score
    dlxjtk_df['dlxjtk_score'] = dlxjtk_df.apply(dlxjtk_func, axis=1)
    dlxjtk_df.sort_values(by='dlxjtk_score', axis=0, ascending=True, inplace=True)

    # Retain only those genes which are considered possible regulators or targets
    annot_df = pd.read_csv(annotation_file, sep='\t', comment='#', index_col=0)
    # annot_df = annot_df[annot_df.apply(lambda row: row.any(), axis=1)]
    # gene_df = pd.DataFrame(index=dlxjtk_df.index)
    # gene_df = gene_df.join(annot_df, how='inner')

    # Filter dlxjtk results by regulators
    tf_annot_df = annot_df.loc[(annot_df["tf_act"] == 1) | (annot_df["tf_rep"] == 1)]
    dlxjtk_tf_df = dlxjtk_df.loc[dlxjtk_df.index.isin(tf_annot_df.index)]
    tf_df = pd.DataFrame(index=dlxjtk_tf_df.index)
    tf_df = tf_df.join(tf_annot_df, how='inner')

    # TODO: Change how cutoff works -> by rank or score (non-unique p-vals)??
    if dlxjtk_cutoff == "None":
        tf_df = pd.DataFrame(index=tf_df.index)
    else:
        tf_df = pd.DataFrame(index=tf_df.iloc[0:int(dlxjtk_cutoff), :].index)

    # write file
    with open(dlxjtk_file, 'w') as f:
        f.write('# DLxJTK regulatory element scores computed on %s\n' % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        f.write("# pyDL file: %s\n" % pydl_file)
        f.write("# pyJTK file: %s\n" % pyjtk_file)
        if rep_numb > 1:
            f.write(f'# P-value combination used: {pval_combine_col}\n')
        dlxjtk_df.to_csv(f, sep='\t', header=True, index=True)

    # TODO: Update to reflect that the cutoff applies to a TF-filtered DLxJTK df
    with open(gene_list_file, 'w') as f:
        f.write('# Allowable gene list based on DLxJTK threshold cutoff on %s\n' % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        f.write("# DLxJTK file: %s\n" % dlxjtk_file)
        f.write("# annotation file: %s\n" % annotation_file)
        f.write("# DLxJTK cutoff: %s\n" % dlxjtk_cutoff)
        tf_df.to_csv(f, sep='\t', header=True, index=True)
