"""
:author: Francis C. Motta
:email: motta<at>fau<dot>edu
:created: 12/07/2018
:updated: 12/07/2018
:copyright: (c) 2018, Francis C. Motta

utility functions supporting other pipeline routines
"""
import os, ast
import sys
from posixpath import basename
import shutil
import time
import numpy as np
import pandas as pd
import scipy.stats as stats
from functools import reduce
from configobj import ConfigObj
import filecmp


def str_to_bool(string):
    if string.lower() in ('true', 'yes', 't', '1'):
        return True
    elif string.lower() in ('false', 'no', 'n', '0'):
        return False
    else:
        raise ValueError


def parse_periods(config_periods):
    # in the case that only multiple periods were specified
    if isinstance(config_periods, list):
        # check if multiple lists were provided.
        # This occurs if there is more than one dataset, which requires multiple list of periods
        if any([i.endswith(']') for i in config_periods]):
            sublist_end_bracket_idx = [i for i, s in enumerate(config_periods) if ']' in s]
            ll_periods = []
            for end_idx in range(len(sublist_end_bracket_idx)):
                if end_idx == 0:
                    sl = config_periods[:sublist_end_bracket_idx[end_idx]+1]
                else:
                    start = sublist_end_bracket_idx[end_idx-1]+1
                    sl = config_periods[start:sublist_end_bracket_idx[end_idx]+1]
                sl = [s.strip('[]') for s in sl]
                ll_periods.append('"' + ', '.join(sl) + '"')
            return ll_periods
        else:
            return '"' + ', '.join(config_periods) + '"'

    # in the case that only one period was specified
    if isinstance(config_periods, str):
        return config_periods


def revert_periods_format(config_periods):
    reformted_periods = list()
    for per_set in config_periods:
        per_set_split = per_set.split(', ')
        for per_set_split_p in per_set_split:
            if per_set_split_p.startswith('"'):
                per_set_split_p = per_set_split_p.replace('"', '[')
            elif per_set_split_p.endswith('"'):
                per_set_split_p = per_set_split_p.replace('"', ']')
            reformted_periods.append(per_set_split_p)
    return reformted_periods


def default_arguments():

    seed = round(time.time())

    def_arg_dict = dict()

    # default settings for DLxJTK
    def_arg_dict['dlxjtk_config'] = dict()
    def_arg_dict['dlxjtk_config']['num_reg'] = 1000000
    def_arg_dict['dlxjtk_config']['num_per'] = 0
    def_arg_dict['dlxjtk_config']['log_trans'] = True
    def_arg_dict['dlxjtk_config']['dlxjtk_cutoff'] = None
    def_arg_dict['dlxjtk_config']['pval_combine_method'] = 'max'

    # default settings for LEMpy
    def_arg_dict['lempy_config'] = dict()
    def_arg_dict['lempy_config']['loss'] = 'euc_loss'
    def_arg_dict['lempy_config']['param_bounds'] = 'tf_param_bounds'
    def_arg_dict['lempy_config']['prior'] = 'uniform_prior'
    def_arg_dict['lempy_config']['normalize'] = True
    def_arg_dict['lempy_config']['inv_temp'] = 1
    def_arg_dict['lempy_config']['seed'] = seed

    def_arg_dict['lempy_config']['minimizer_params'] = dict()
    def_arg_dict['lempy_config']['minimizer_params']['niter'] = 200
    def_arg_dict['lempy_config']['minimizer_params']['T'] = 1
    def_arg_dict['lempy_config']['minimizer_params']['stepsize'] = .5
    def_arg_dict['lempy_config']['minimizer_params']['interval'] = 10
    def_arg_dict['lempy_config']['minimizer_params']['disp'] = False
    def_arg_dict['lempy_config']['minimizer_params']['seed'] = seed

    # default settings for seed network generator
    def_arg_dict['netgen_config'] = dict()
    def_arg_dict['netgen_config']['edge_score_column'] = 'pld'
    def_arg_dict['netgen_config']['edge_score_threshold'] = None
    def_arg_dict['netgen_config']['num_edges_for_list'] = None
    def_arg_dict['netgen_config']['seed_threshold'] = None
    def_arg_dict['netgen_config']['num_edges_for_seed'] = None

    # default settings for network perturbations
    def_arg_dict['netper_config'] = dict()
    def_arg_dict['netper_config']['compressed_output'] = True
    def_arg_dict['netper_config']['DSGRN_optimized'] = True
    def_arg_dict['netper_config']['time_to_wait'] = 30
    def_arg_dict['netper_config']['random_seed'] = seed
    def_arg_dict['netper_config']['filters'] = {"is_connected" : {}}


    # default settings for DSGRN queries
    def_arg_dict['netquery_config'] = dict()
    def_arg_dict['netquery_config']["tsfile_is_row_format"] = True
    def_arg_dict['netquery_config']["domain"] = True
    def_arg_dict['netquery_config']["stablefc"] = False
    def_arg_dict['netquery_config']["count"] = True
    def_arg_dict['netquery_config']["epsilons"] = [0.1,0.05,0.10]

    return def_arg_dict


def gen_dlxjtk_config(co):
    def_arg_dict = default_arguments()
    dlxjtk_config = ConfigObj(co['dlxjtk_arguments'])

    # Pass in pipeline config arguments needed by DLxJTK
    dlxjtk_config['data_file'] = co['data_file']
    dlxjtk_config['output_dir'] = os.path.join(co['output_dir'], 'node_finding_' + co['dlxjtk_datetime'])
    dlxjtk_config['annotation_file'] = co['annotation_file']

    # Update dlxjtk configuration values
    dlxjtk_config['periods'] = parse_periods(dlxjtk_config['periods'])
    dlxjtk_config['dl_output_file'] = os.path.join(dlxjtk_config['output_dir'], 'pydl_results.tsv')
    dlxjtk_config['jtk_output_file'] = os.path.join(dlxjtk_config['output_dir'], 'pyjtk_results.tsv')
    dlxjtk_config['dlxjtk_output_file'] = os.path.join(dlxjtk_config['output_dir'], 'dlxjtk_results.tsv')
    dlxjtk_config['gene_list_file'] = os.path.join(dlxjtk_config['output_dir'], 'gene_list.tsv')
    dlxjtk_config['verbose'] = co['verbose']
    dlxjtk_config['num_proc'] = co['num_proc']

    # Populate DL and JTK config file with defaults if necessary
    for arg in ['num_reg', 'num_per', 'log_trans', 'dlxjtk_cutoff', 'pval_combine_method']:
        if arg not in dlxjtk_config:
            dlxjtk_config[arg] = def_arg_dict['dlxjtk_config'][arg]

    dlxjtk_config.filename = os.path.join(dlxjtk_config['output_dir'] ,'dlxjtk_%s_config.txt' % co['dlxjtk_datetime'])

    return dlxjtk_config


def gen_lempy_config(co):
    def_arg_dict = default_arguments()
    lempy_config = ConfigObj(co['lempy_arguments'])

    # Walk the config file and reduce the subsections by one
    def reduce_subsections(section, key):
        if section.depth > 1:
            section.depth = section.depth - 1

    lempy_config.walk(reduce_subsections)

    # Pass in pipeline config arguments needed by LEMpy
    lempy_config['data_files'] = co['data_file']
    lempy_config['verbose'] = co['verbose']
    lempy_config['num_proc'] = co['num_proc']
    lempy_config['annotation_file'] = co['annotation_file']

    # Populate LEMpy config file with defaults as necessary
    for arg in ['loss', 'param_bounds', 'prior', 'normalize', 'inv_temp', 'seed']:
        if arg not in lempy_config:
            lempy_config[arg] = def_arg_dict['lempy_config'][arg]

    if "minimizer_params" not in lempy_config:
        lempy_config['minimizer_params'] = def_arg_dict['lempy_config']['minimizer_params']
    else:
        for arg in ['niter', 'T', 'stepsize', 'interval', 'disp', 'seed']:
            if arg not in lempy_config['minimizer_params']:
                lempy_config['minimizer_params'][arg] = def_arg_dict['lempy_config']['minimizer_params'][arg]

    # Specify the default output location needed for the next step
    lempy_config['output_dir'] = os.path.join(co['output_dir'], 'edge_finding_' + co['lempy_datetime'])

    # Load the allowable gene list file and gene annotation file
    gene_df = pd.read_csv(lempy_config['gene_list_file'], sep='\t', comment='#', index_col=0)
    annot_df = pd.read_csv(lempy_config['annotation_file'], sep='\t', comment='#', index_col=0)
    regs = list(annot_df.columns.values)
    regs.remove('target')

    # Retain only those genes which are considered possible regulators or targets
    annot_df = annot_df[annot_df.apply(lambda row: row.any(), axis=1)]
    gene_df = annot_df.join(gene_df, how='inner')

    # Sort remaining genes by DLxJTK score
    genes = gene_df.index.values

    # Populate LEMpy config file with targets and regulators sections based on DLxJTK output and gene annotations
    lempy_config['targets'] = dict()
    lempy_config['regulators'] = dict()
    for gene in genes:
        if gene_df.loc[gene, 'target']:
            # Gene is an allowed target
            lempy_config['targets'][gene] = ''

        lempy_config['regulators'][gene] = []
        for reg in regs:
            if gene_df.loc[gene, reg]:
                # Gene is allowed this model of regulation
                lempy_config['regulators'][gene].append(reg)

        if len(lempy_config['regulators'][gene]) == 0:
            lempy_config['regulators'].pop(gene, None)

    lempy_config.filename = os.path.join(lempy_config['output_dir'], 'lempy_%s_config.txt' % co['lempy_datetime'])

    return lempy_config


def gen_netgen_config(co):
    def_arg_dict = default_arguments()
    netgen_config = ConfigObj(co['netgen_arguments'])

    # Populate seed network generation config file with defaults as necessary
    for arg in ['edge_score_column', 'edge_score_threshold', 'seed_threshold', 'num_edges_for_seed', 'num_edges_for_list']:
        if arg not in netgen_config:
            netgen_config[arg] = def_arg_dict['netgen_config'][arg]

    # Specify the default output location needed for the next step
    netgen_config['output_dir'] = os.path.join(co['output_dir'], 'seed_network_' + co['netgen_datetime'])
    # netgen_config['edge_list_file'] = os.path.join(netgen_config['output_dir'], 'edge_file.txt')
    # netgen_config['node_list_file'] = os.path.join(netgen_config['output_dir'], 'node_file.txt')
    # netgen_config['seed_net_file'] = os.path.join(netgen_config['output_dir'], 'seed_network_file.txt')

    netgen_config.filename = os.path.join(netgen_config['output_dir'], 'netgen_%s_config.txt' % co['netgen_datetime'])

    return netgen_config


def gen_netper_config(co):
    def_arg_dict = default_arguments()
    netper_config = ConfigObj(co['netper_arguments'])

    # Populate network perturbations config file with defaults as necessary
    for arg in ['filters', 'compressed_output', 'DSGRN_optimized', 'time_to_wait', 'random_seed']:
        if arg not in netper_config:
            netper_config[arg] = def_arg_dict['netper_config'][arg]

    if "networkfile" not in netper_config:
        if "seed_net_file" in netper_config:
            netper_config["networkfile"] = netper_config["seed_net_file"]
        else:
            raise ValueError("Missing seed network file in network finding configuration.")

    if "edgefile" not in netper_config:
        if "edge_list_file" in netper_config:
            netper_config["edgefile"] = netper_config["edge_list_file"]
        else:
            raise ValueError("Missing edge list file in network finding configuration.")

    if "nodefile" not in netper_config:
        if "node_list_file" in netper_config:
            netper_config["nodefile"] = netper_config["node_list_file"]
        else:
            raise ValueError("Missing node list file in network finding configuration.")

    # convert data types for json output
    try:
        netper_config['range_operations'] = [int(i) for i in netper_config['range_operations']]
    except:
        raise ValueError("'range_operations' is a required argument for network finding.")

    # Specify the default output location needed for the next step
    netper_config['datetime'] = co['netper_datetime']
    netper_config['resultsdir'] = os.path.join(co['output_dir'], 'network_finding_' + co['netper_datetime'])
    # netper_config['seed_net_file'] = os.path.join(netper_config['resultsdir'], 'seed_net_file.txt')

    netper_config.filename = os.path.join(netper_config['resultsdir'], 'netper_%s_config.txt' % co['netper_datetime'])

    return netper_config


def gen_netquery_config(co):
    def_arg_dict = default_arguments()
    netquery_config = ConfigObj(co['netquery_arguments'])


    # Populate network perturbations config file with defaults as necessary
    for arg in [ 'count', 'epsilons','domain','stablefc','tsfile_is_row_format' ]:
        if arg not in netquery_config:
            netquery_config[arg] = def_arg_dict['netquery_config'][arg]

    # convert data types from strings to floats
    netquery_config['epsilons'] = [float(i) for i in netquery_config['epsilons']]
    netquery_config['timeseriesfname'] = co['data_file']

    # Specify the default output location
    netquery_config['datetime'] = co['netquery_datetime']
    netquery_config['resultsdir'] = os.path.join(co['output_dir'], 'network_queries_' + co['netquery_datetime'])

    netquery_config.filename = os.path.join(netquery_config['resultsdir'], 'netquery_%s_config.txt' % co['netquery_datetime'])

    return netquery_config


def combine_pvals_results(file_rep_dict, dlxjtk_config, per_alg):

    # read each pydl results into individual dataframes
    rep_df_list = [pd.read_csv(rep_file, sep='\t', comment='#', index_col=0) for rep_file in file_rep_dict.keys()]

    # for each dataframe, append replicate number to end of each column name
    for rep, rep_df in enumerate(rep_df_list):
        rep_df.columns = [f'{colname}_rep{rep + 1}' for colname in rep_df.columns]

    # merge all dataframes together on the index/gene names
    rep_merged = reduce(lambda left, right: pd.merge(left, right, left_index=True, right_index=True, how='outer'), rep_df_list)

    if per_alg == 'dl':
        rep_merged = combine_pydl_results(rep_merged)

    elif per_alg == 'jtk':
        rep_merged = combine_pyjtk_results(rep_merged)

    with open(dlxjtk_config[f'{per_alg}_output_file'], 'w') as out_file:
        out_file.write(f'# Merged py{per_alg} periodicity scores of replicate time series\n')
        out_file.write("# Fisher's method used for combining p-values\n")
        out_file.write(f'# py{per_alg} Results Files, Replicate Number:  %s\n' % file_rep_dict)
        out_file.write('# Period(s) of oscillation tested:  %s\n' % dlxjtk_config['periods'])
        out_file.write('# Number of random curves generated from background:  %i\n' % dlxjtk_config['num_reg'])
        out_file.write('# Number of curve permutations:  %i\n' % dlxjtk_config['num_per'])
        rep_merged.to_csv(out_file, sep='\t')


def combine_pyjtk_results(merged_df):

    pval_rep_def = merged_df.loc[:, [colname for colname in merged_df.columns if colname.startswith('p-value_rep')]]
    merged_df['p-value_min'] = pval_rep_def.apply(lambda row: min(row), axis=1)
    merged_df['p-value_max'] = pval_rep_def.apply(lambda row: max(row), axis=1)
    merged_df['p-value_fisher'] = pval_rep_def.apply(lambda row: stats.combine_pvalues(row, method='fisher')[1], axis=1)

    return merged_df


def combine_pydl_results(merged_df):

    for pval_col in ['p_per', 'p_reg']:
        pval_rep_def = merged_df.loc[:, [colname for colname in merged_df.columns if colname.startswith(f'{pval_col}_rep')]]
        merged_df[f'{pval_col}_min'] = pval_rep_def.apply(lambda row: min(row), axis=1)
        merged_df[f'{pval_col}_max'] = pval_rep_def.apply(lambda row: max(row), axis=1)
        merged_df[f'{pval_col}_fisher'] = pval_rep_def.apply(lambda row: stats.combine_pvalues(row, method='fisher')[1], axis=1)

    # replace any p-values of 0 with the minimum between either the smallest nonzero p-value or .001
    merged_df[merged_df == 0] = np.min([merged_df[merged_df != 0].min().min(), .001])

    for pval_col in ['p_per', 'p_reg']:
        # normalize min p-values by the median value
        merged_df[f'{pval_col}_min_norm'] = merged_df[f'{pval_col}_min'] / merged_df[f'{pval_col}_min'].median()
        # normalize max p-values by the median value
        merged_df[f'{pval_col}_max_norm'] = merged_df[f'{pval_col}_max'] / merged_df[f'{pval_col}_max'].median()
        # normalize fisher combined p-values by the median value
        merged_df[f'{pval_col}_fisher_norm'] = merged_df[f'{pval_col}_fisher'] / merged_df[f'{pval_col}_fisher'].median()

    for cm in ['min', 'max', 'fisher']:
        # Calculate DL score for p-value combining method
        merged_df[f'p_tot_{cm}'] = merged_df[f'p_reg_{cm}_norm'] * merged_df[f'p_per_{cm}_norm']
        merged_df[f'dl_score_{cm}'] = merged_df[f'p_tot_{cm}'] * (1 + (merged_df[f'p_reg_{cm}_norm'] / 0.001) ** 2) * (
                1 + ((merged_df[f'p_per_{cm}_norm'] / 0.001) ** 2))

    return merged_df


def make_annot_file(data_file_list, output_dir):

    # make a single annotation file that encompasses genes in all files listed in data_file config argument
    union_gene_list = list()
    # loop through each datasets and append the index (list of genes) to union_gene_list
    for single_data_file in data_file_list:
        single_data_df = pd.read_csv(single_data_file, comment='#', sep='\t', index_col=0)
        union_gene_list += single_data_df.index.tolist()
    # remove duplicate gene names
    unique_gene_list = list(set(union_gene_list))
    # create annotation df where every gene is a transcription factor as well as a target
    annot_df = pd.DataFrame(1, index=unique_gene_list, columns=['tf_act', 'tf_rep', 'target'])
    annot_df.index.name = 'node'
    # save annotation df to the directory specificed in output_dir
    with open(f'{output_dir}/annotation_file.tsv', 'w') as out_file:
        out_file.write(f'# Annotation file created from the unique union of genes in data files: {", ".join(data_file_list)}\n')
        out_file.write("# All genes have been annotated as transcription factors as well as targets\n")
        annot_df.to_csv(out_file, sep='\t')

    # return annot file path
    return f'{output_dir}/annotation_file.tsv'


def make_inputs_directory(config):

    input_dir_path = os.path.join(config['output_dir'], 'input_files')
    existing_input_files = os.listdir(input_dir_path)
    
    # list containing all possible user-specificed files
    # potential_input_files = ['annotation_file', 
    #                         'lempy_arguments/gene_list_file',
    #                         'lempy_arguments/ground_truth_file',
    #                         'netgen_arguments/edge_score_file',
    #                         'netper_arguments/edge_list_file',
    #                         'netper_arguments/node_list_file',
    #                         'netper_arguments/seed_net_file',
    #                         'netquery_arguments/timeseriesfname',
    #                         'netquery_arguments/networkfile']
    potential_input_files = ['data_file', 'annotation_file',
                            'lempy_arguments/gene_list_file',
                            'lempy_arguments/ground_truth_file',
                            'netgen_arguments/edge_score_file',
                            'netper_arguments/edge_list_file',
                            'netper_arguments/node_list_file',
                            'netper_arguments/seed_net_file',
                            'netquery_arguments/timeseriesfname',
                            'netquery_arguments/networkfile']

    def file_check_and_save(file):
        # check if a file with the same name exists in the inputs directory already.
        # if it does then check if contents are the same. If they are the same, don't copy the file.
        # If the contents are different (and the files have the same nem), stop the program,
        # alert the user, and suggest changing the name of file.
        dupname = os.path.basename(file)
        file_inputs_path = os.path.join(input_dir_path, dupname)
        if dupname not in existing_input_files:
            shutil.copyfile(file, file_inputs_path)
            return
        else:
            if filecmp.cmp(file, file_inputs_path):
                return
            else:
                print()
                print(f'ERROR: {dupname} already exists in the input_files folder.')
                print(f'There seems to be a file with the exact same name but contains different content as {dupname} in the input_files directory.')
                print(f'Suggestion: Rename the {dupname} file and update your configuration file.')
                return True

    # var for knowing if to stop the program or not
    error_check = False

    # loop over potential user-specificed files
    for iarg in potential_input_files:
        dfile = str()
        # check if potential user-specified file is a top level argument in config
        if '/' not in iarg:
            dfile = config[iarg]
        else:
            # else the potential user-specified file is a second-level argument
            step, arg = iarg.split('/')
            try:
                dfile = config[step][arg]
            except:
                pass 
        
        # check if iarg is a list of files
        # this will be either [data_file] or [netquery_arguments][timeseriesfname]
        if isinstance(dfile, list):
            for single_dfile in dfile:
                # check if file exists (in name and content)
                is_dup = file_check_and_save(single_dfile)
                # if the file exists (same name and content), the change error_check to True
                if is_dup:
                    error_check = is_dup
        elif dfile:
            # check if file exists (in name and content)
            is_dup = file_check_and_save(dfile)
            # if the file exists (same name and content), the change error_check to True
            if is_dup:
                error_check = is_dup
        else:
            pass
    
    # if a user-specified file already exists (in name and content) in the input_files directory, then kill the program.
    if error_check:
        sys.exit()
    else:
        return
