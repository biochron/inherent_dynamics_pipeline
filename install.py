"""
Installs the package requirements and Git repositories needed for the Data-to-Network Pipeline
"""
import os, subprocess


def rmdir(dir):
    # change permissions of all files in dir to ensure automatic deletion
    for root, dirs, files in os.walk(dir):
        for d in dirs:
            os.chmod(os.path.join(root, d), 0o777)
        for f in files:
            os.chmod(os.path.join(root, f), 0o777)

    # completely delete dir and all its contents
    if dir[-1] == os.sep: dir = dir[:-1]
    files = os.listdir(dir)
    for file in files:
        if file == '.' or file == '..': continue
        path = dir + os.sep + file
        if os.path.isdir(path):
            rmdir(path)
        else:
            os.unlink(path)
    os.rmdir(dir)

# Clone the necessary repositories
for dir in ['pydl', 'pyjtk', 'lempy', 'min_interval_posets', 'dsgrn_net_gen', 'dsgrn_net_query','dsgrn_utilities', 'DSGRN']:
    if os.path.isdir(dir):
        rmdir(dir)

os.system('git clone https://gitlab.com/biochron/pydl.git')
os.system('git clone https://gitlab.com/biochron/pyjtk.git')
os.system('git clone https://gitlab.com/biochron/lempy.git')
os.system('git clone https://github.com/breecummins/min_interval_posets.git')
os.system('git clone https://github.com/breecummins/dsgrn_net_gen.git')
os.system('git clone https://github.com/breecummins/dsgrn_net_query.git')
os.system('git clone https://github.com/breecummins/dsgrn_utilities.git')

# install modules that require installation
os.chdir('pyjtk')
subprocess.call(['bash install.sh'], shell=True)
os.chdir('../pydl')
subprocess.call(['bash install.sh'], shell=True)
os.chdir('../min_interval_posets')
subprocess.call(['bash install.sh'], shell=True)
os.chdir('../dsgrn_utilities')
subprocess.call(['bash install.sh'], shell=True)
os.chdir('../dsgrn_net_gen')
subprocess.call(['bash install.sh'], shell=True)
os.chdir('../dsgrn_net_query')
subprocess.call(['bash install.sh'], shell=True)

os.chdir('../min_interval_posets')
subprocess.call(['pytest'], shell=True)
os.chdir('../dsgrn_net_gen/tests')
subprocess.call(['pytest'], shell=True)
os.chdir('../../dsgrn_net_query/tests')
subprocess.call(['pytest'], shell=True)
os.chdir('../../dsgrn_utilities/tests')
subprocess.call(['pytest'], shell=True)
os.chdir("../..")