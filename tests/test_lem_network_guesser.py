import sys, subprocess
sys.path.insert(1, '../src')
import lem_network_guesser as lng
import dsgrn_net_gen.fileparsers as fp
from pathlib import Path

Path("temp_results").mkdir(exist_ok=True)


def test1():
    nodefile,edgefile,seedfile = lng.generate_lem_networks("lem_scores_test.tsv","norm_loss","temp_results",num_top_edges = 3, seed_threshold=None, num_edges_for_list=15, edge_threshold=None)
    edges = fp.parseEdgeFile(edgefile)
    no_seed = set(
        [("D", "F", "a"), ("A", "F", "r"), ("F", "F", "a"), ("C", "F", "a"), ("B", "F", "a"), ("A", "F", "a")])
    assert(set(edges) == no_seed)
    seed = open(seedfile).read()
    assert(seed == "B :  : E\nC :  : E\nE :  : E\nF : (~B)(~C)(~E) : E\n")


def test1_5():
    nodefile,edgefile,seedfile = lng.generate_lem_networks(["lem_scores_test.tsv","lem_scores_test3.tsv"],"norm_loss","temp_results",num_top_edges = 3, seed_threshold=None, num_edges_for_list=15, edge_threshold=None)
    edges = fp.parseEdgeFile(edgefile)
    no_seed = set(
        [("D", "F", "a"), ("A", "F", "r"), ("F", "F", "a"), ("C", "F", "a"), ("B", "F", "r"), ("A", "F", "a")])
    assert(set(edges) == no_seed)
    seed = open(seedfile).read()
    assert(seed == "B :  : E\nC :  : E\nE :  : E\nF : (B)(~C)(~E) : E\n")


def test2():
    nodefile,edgefile,seedfile = lng.generate_lem_networks("lem_scores_test.tsv","pld","temp_results",num_top_edges = 3, seed_threshold=None, num_edges_for_list=15, edge_threshold=None)
    edges = fp.parseEdgeFile(edgefile)
    no_seed = set([("E","F","r"),("A","F","r"),("F","F","a"),("C","F","a"),("B","F","a"),("A","F","a")])
    assert(set(edges) == no_seed)
    seed = open(seedfile).read()
    assert(seed == "B :  : E\nC :  : E\nD :  : E\nF : (D)(~B)(~C) : E\n")


def test2_5():
    nodefile,edgefile,seedfile = lng.generate_lem_networks("lem_scores_test.tsv","pld","temp_results",num_top_edges = 3, seed_threshold=None, num_edges_for_list=1, edge_threshold=None)
    edges = fp.parseEdgeFile(edgefile)
    assert(set(edges) == set([("E","F","r")]))


def test3():
    nodefile,edgefile,seedfile = lng.generate_lem_networks("lem_scores_test.tsv","norm_loss","temp_results",num_top_edges = None, seed_threshold=0.6, num_edges_for_list=None, edge_threshold=0.6)
    edges = fp.parseEdgeFile(edgefile)
    assert(set(edges) == set([]))
    seed = open(seedfile).read()
    assert(seed == "B :  : E\nC :  : E\nE :  : E\nF : (~B)(~C)(~E) : E\n")


def test4():
    nodefile,edgefile,seedfile = lng.generate_lem_networks("lem_scores_test.tsv","pld","temp_results",num_top_edges = None, seed_threshold=2e-9, num_edges_for_list=None, edge_threshold=0)
    edges = fp.parseEdgeFile(edgefile)
    no_seed = set([("F","F","a"),("C","F","a"),("B","F","a"),("A","F","a")])
    assert(set(edges) == no_seed)
    seed = open(seedfile).read()
    assert(seed == "A :  : E\nB :  : E\nC :  : E\nD :  : E\nE :  : E\nF : (D)(~A)(~B)(~C)(~E) : E\n")


def test5():
    # test multi-edge function
    order = ">"
    all_sources, all_targets, all_regs, all_scores = lng.parse_edge_file("lem_scores_test2.tsv","pld", "#")
    edges = {t: all_scores[0][k] for k, t in enumerate(zip(all_sources[0], all_targets[0], all_regs[0]))}
    score_dict = lng.remove_self_repressors(edges)
    no_sr = set(score_dict.keys())
    no_self_rep = set([("B","F","r"),("C","F","r"),("E","F","r"),("D","F","a"),("A","F","r"),("F","F","a"),("C","F","a"),("B","F","a"),("A","F","a")])
    assert(no_sr == no_self_rep)
    score_dict2 = lng.remove_multiedges(score_dict,order)
    no_multi = set([("B","F","r"),("C","F","r"),("E","F","r"),("D","F","a"),("A","F","r"),("F","F","a")])
    no_me = set(score_dict2.keys())
    assert(no_me == no_multi)
    subprocess.call(["rm", "-r", "temp_results/"])


if __name__ == "__main__":
    test1()