import pandas as pd
import os

def make_df(fname):
    df = pd.read_csv(fname, sep = "\t", comment = "#",index_col=0)
    df.drop(df.columns.difference(['p_reg_norm',"p_reg_max_norm"]), 1, inplace=True)
    return df.T.to_dict('list')


def compare_scores(fnames):
    reg_scores = {}
    for f in fnames:
        df_dict = make_df(f)
        for key,val in df_dict.items():
            if key not in reg_scores:
                reg_scores[key] = []
            reg_scores[key].append(val[0])
    return reg_scores


if __name__ == "__main__":
    dirs = ["results/node_finding_master_branch","results/node_finding_moseley_dev_branch_1ts","results/node_finding_moseley_dev_branch_2ts"]
    fnames = [os.path.join(d,"pydl_results.tsv") for d in dirs]

    cs = compare_scores(fnames)

    for it in cs.items():
        print(it)


